<?php


require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Name : ". $sheep->name . "<br>";
echo "Legs : ". $sheep->legs . "<br>";
echo "cold blooded : ". $sheep->cold_blooded . "<br><br>";

$kodok = new frog("buduk");
echo "Name : ". $kodok->name . "<br>";
echo "Legs : ". $kodok->legs . "<br>";
echo "cold blooded : ". $kodok->cold_blooded . "<br>";
echo $kodok->katak("Hop Hop"). "<br><br>";

$sunggokong = new ape("kera sakti");
echo "Name : ". $sunggokong->name . "<br>";
echo "Legs : ". $sunggokong->legs . "<br>";
echo "cold blooded : ". $sunggokong->cold_blooded . "<br>";
echo $sunggokong->ape("Auoo"). "<br><br>";


?>