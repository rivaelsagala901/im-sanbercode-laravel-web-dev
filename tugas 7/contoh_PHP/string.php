<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh soal</h2>
    <?php
        echo "<h3>Contoh Soal 1</h3>";
        $kalimat1 = "PHP is never old";
        echo "Kalimat pertama : " . $kalimat1 . "<br>";
        echo "Panjang kalimat : " . strlen($kalimat1) . "<br>";
        echo "Jumlah Kata : " . str_word_count($kalimat1) . "<br><br>";


        echo "<h3>Contoh Soal 2</h3>";
        $kalimat2 = "I love PHP";
        echo "Kalimat kedua : " . $kalimat2. "<br>";
        echo "Kata pertama : " . substr($kalimat2, 0, 1) . "<br>";
        echo "Kata kedua : " . substr($kalimat2, 2, 5) . "<br>";
        echo "Kata ketiga : " . substr($kalimat2, 7, 10) . "<br><br>";


        echo "<h3>Contoh Soal 2</h3>";
        $kalimat3 = "PHP is old but sexy!";
        echo "Kalimat ketiga : " . $kalimat3. "<br>";
        echo "Mengganti kata keempat : " . str_replace("sexy","awesome",$kalimat3);
    
    ?>
</body>
</html>